package ldh.maker;

import ldh.maker.component.MobileContentUiFactory;
import ldh.maker.util.UiUtil;

/**
 * Created by ldh on 2017/4/24.
 */
public class MobileMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new MobileContentUiFactory());
        UiUtil.setType("mobile");
    }

    public static void main(String[] args) throws Exception {
        startDb(null);
        launch(args);
    }
}
