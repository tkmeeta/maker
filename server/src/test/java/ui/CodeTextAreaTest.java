package ui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import ldh.maker.component.CodeTextArea;
import ldh.maker.util.MvnUtil;

/**
 * Created by ldh on 2018/3/4.
 */
public class CodeTextAreaTest extends Application {

    @Override
    public void start(Stage stage) {
        VBox box = new VBox();
        Button b = new Button("run spring");
        b.setOnAction(e->{
            Thread thread = new Thread(()->runSpring());
            thread.setDaemon(true);
            thread.start();
        });
        box.getChildren().addAll(b, new CodeTextArea(sampleCode, false));
        Scene scene = new Scene(box);
        stage.setTitle("Table View Sample");
        stage.setWidth(450);
        stage.setHeight(500);

        scene.getStylesheets().add("/styles/java-keywords.css");
//        scene.getStylesheets().add(CodeTextAreaTest.class.getResource("java-keywords.css").toExternalForm());
        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest(e-> {
            System.exit(-1);
        });
    }

    private void runSpring() {
        MvnUtil.runMvn("clean spring-boot:run", "E:\\git\\maker\\code\\school\\school-jsp\\pom.xml", (s, t)->{
            System.out.println("msg:" + s);
            if (t != null) {
                t.printStackTrace();
            }
        });
    }

    public static void main(String[] args) {
        launch(args);
    }

    private static final String sampleCode = String.join("\n", new String[] {
            "package com.example;",
            "",
            "import java.util.*;",
            "",
            "public class Foo extends Bar implements Baz {",
            "",
            "    /*",
            "     * multi-line comment",
            "     */",
            "    public static void main(String[] args) {",
            "        // single-line comment",
            "        for(String arg: args) {",
            "            if(arg.length() != 0)",
            "                System.out.println(arg);",
            "            else",
            "                System.err.println(\"Warning: empty string as argument\");",
            "        }",
            "    }",
            "",
            "}"
    });
}
