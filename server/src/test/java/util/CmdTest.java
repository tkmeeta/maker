package util;

import java.io.DataInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.StringReader;
import java.util.concurrent.TimeUnit;

public class CmdTest {

    public static void main(String[] args) {
        String path = "E:\\git\\maker\\code\\school\\school-jsp\\pom.xml";
        try {
            Process process = Runtime.getRuntime().exec("cmd /k start mvn -f " + path + " clean spring-boot:run -Pdev");
            InputStream in = process.getInputStream();
            DataInputStream reader = new DataInputStream(in);
            String str = null;
            while ((str = reader.readUTF())!= null) {
                System.out.println(str);
            }
            in.close();
            process.waitFor(10, TimeUnit.SECONDS);
            process.destroy();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
