package ldh.maker.db;

import de.jensd.fx.glyphs.GlyphIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import de.jensd.fx.glyphs.materialicons.MaterialIcon;
import de.jensd.fx.glyphs.materialicons.MaterialIconView;
import javafx.application.Platform;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.paint.Color;
import ldh.maker.constants.TreeNodeTypeEnum;
import ldh.maker.controller.MainController;
import ldh.maker.util.UiUtil;
import ldh.maker.vo.TreeNode;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ldh on 2017/2/26.
 */
public class TreeNodeDb {

    public static void save(TreeNode treeNode) throws SQLException {
        Connection connection = UiUtil.H2CONN;
        String sql = "insert into tree_node(name, text, type, parent_id) values(?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, treeNode.getData().toString());
        statement.setString(2, treeNode.getText());
        statement.setString(3, treeNode.getType().name());
        statement.setInt(4, treeNode.getParent().getId());
        statement.executeUpdate();
        ResultSet rs = statement.getGeneratedKeys();
        if(rs.next()){
            treeNode.setId(rs.getInt(1));
        }
        statement.close();
    }

    public static void initProject(TreeView<TreeNode> projectTree) throws SQLException {
        Connection connection = UiUtil.H2CONN;
        Statement statement = connection.createStatement();
        String sql = "select * from tree_node where parent_id = 1";
        ResultSet rs = statement.executeQuery(sql);
        while(rs.next()) {
            TreeNode tn = new TreeNode(TreeNodeTypeEnum.valueOf(rs.getString("type")), rs.getString("text"), MainController.root.getValue());
            tn.setId(Integer.valueOf(rs.getString("id")));
            TreeItem<TreeNode> treeItem = new TreeItem<>(tn);
            TreeNodeDb.setGraphic(treeItem);
            MainController.root.getChildren().add(treeItem);
        }
        statement.close();
    }

    public static void initNode(TreeItem<TreeNode> treeItem) throws SQLException {
        List<TreeNode> treeNodeList = loadTreeNode(treeItem.getValue());

        Platform.runLater(()->{
            setGraphic(treeItem);

            for (TreeNode treeNode : treeNodeList) {
                TreeItem<TreeNode> treeItem1 = new TreeItem<>(treeNode);
                setGraphic(treeItem1);
                treeItem.getChildren().add(treeItem1);
            }
            treeItem.setExpanded(true);

            for(TreeItem<TreeNode> tn : treeItem.getChildren()) {
                if (tn.getValue().getType() == TreeNodeTypeEnum.DB_CONNECTION) {
                    try {
                        DbConnectionDb.initDbConnection(tn.getValue());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    public static List<TreeNode> loadTreeNode(TreeNode treeNode) throws SQLException {
        List<TreeNode> treeNodeList = new ArrayList<>();
        Connection connection = UiUtil.H2CONN;
        Statement statement = connection.createStatement();
        String sql = "select * from tree_node where parent_id = " + treeNode.getId();
        ResultSet rs = statement.executeQuery(sql);
        while(rs.next()) {
            TreeNode tn = new TreeNode(TreeNodeTypeEnum.valueOf(rs.getString("type")), rs.getString("text"), treeNode);
            tn.setId(Integer.valueOf(rs.getString("id")));
            treeNodeList.add(tn);
        }
        statement.close();
        return treeNodeList;
    }

    public static void deleteTreeNode(TreeNode treeNode) throws SQLException {
        Connection connection = UiUtil.H2CONN;
        Statement statement = connection.createStatement();
        String sql = "delete tree_node where id = " + treeNode.getId();
        statement.execute(sql);
        statement.close();
    }

    public static void setGraphic(TreeItem<TreeNode> graphic) {
        TreeNode treeNode = graphic.getValue();
        if (graphic.getGraphic() != null) return;
        GlyphIcon glyphIcon = null;
        if (treeNode.getType() == TreeNodeTypeEnum.PROJECT) {
            glyphIcon = new FontAwesomeIconView(FontAwesomeIcon.HOME);
        } else if (treeNode.getType() == TreeNodeTypeEnum.DATABASE) {
            glyphIcon = new MaterialIconView(MaterialIcon.DATA_USAGE);
        } else if (treeNode.getType() == TreeNodeTypeEnum.DB_CONNECTION) {
            glyphIcon = new MaterialDesignIconView(MaterialDesignIcon.BLUETOOTH_CONNECT);
        } else if (treeNode.getType() == TreeNodeTypeEnum.DB) {
            glyphIcon = new FontAwesomeIconView(FontAwesomeIcon.DATABASE);
        } else if (treeNode.getType() == TreeNodeTypeEnum.DB_TABLE) {
            glyphIcon = new FontAwesomeIconView(FontAwesomeIcon.TABLE);
        }
        GlyphIcon setGlyhIcon = glyphIcon;
        if (setGlyhIcon == null) return;
        setGlyhIcon.setFill(Color.BLUE);
        setGlyhIcon.setGlyphSize(22);
        graphic.setGraphic(setGlyhIcon);
    }
}