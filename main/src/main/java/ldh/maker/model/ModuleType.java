package ldh.maker.model;

import ldh.maker.*;

public enum ModuleType {
    WEB("web", "web", MicroServerMain.class, "生成一个基于spring boot的后台接口程序，是很多其他框架的基础"),
    WEB_EASYUI("web easyui", "easyui", EasyuiWebMain.class, "是一个基于easyui传统ui的后端框架"),
    WEB_jsp("web jsp", "jsp", BootstrapWebMain.class, "是一个基于bootstrap + jsp传统的后端框架"),
    WEB_freemarker("web freemarker", "freemarker", FreemarkerWebMain.class, "是一个基于freemarker的后端框架, 使用spring boot推荐"),
    DESKTOP_SPRING("desktop spring", "descktop1", SpringDeskMain.class, "桌面程序，包含了后端逻辑代码，并使用spring管理ioc"),
    DESKTOP_CLIENT("desktop client", "desktop", DeskMain.class, "桌面程序，只注重UI，需要和WEB接口一起使用"),
    FLUTTER_MOBILE("flutter mobile", "flutter", FlutterMain.class, "基于Flutter跨mobile程序，需要和WEB接口一起使用"),
    NODEJS("nodejs", "nodejs", NodejsWebMain.class, "基于nodejs + vue的前端程序，需要和WEB接口一起使用"),
    VERTX("vertx", "vertx", VertxMain.class, "基于vertx接口程序，注重性能可以选择这个"),
    SpringBoot("Spring Cloud", "Spring Cloud", SpringCloudCreateMain.class, "spring cloud 相关项目"),
    ;

    private String name;
    private String dbName;
    private Class clazz;
    private String desc;

    private ModuleType(String name, String dbName, Class clazz, String desc) {
        this.name = name;
        this.dbName = dbName;
        this.clazz = clazz;
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public Class getClazz() {
        return clazz;
    }

    public String getDesc() {
        return desc;
    }

    public String getDbName() {
        return dbName;
    }

    private static String webdesc = "生成一个基于spring boot的后台接口程序，是很多其他框架的基础";
    private static String webEasyuidesc = "是一个基于easyui传统ui的后端框架";
    private static String webJspdesc = "是一个基于bootstrap + jsp传统的后端框架";
    private static String webFreemarkerdesc = "是一个基于freemarker的后端框架, 使用spring boot推荐";
    private static String desktopSpringdesc = "桌面程序，包含了后端逻辑代码，并使用spring管理ioc";
    private static String desktopClientdesc = "桌面程序，只注重UI，需要和WEB接口一起使用";
    private static String flutterMobileDesc = "基于Flutter跨mobile程序，需要和WEB接口一起使用";
    private static String nodejsDesc = "基于nodejs + vue的前端程序，需要和WEB接口一起使用";
    private static String vertxDes = "基于vertx接口程序，注重性能可以选择这个";
}