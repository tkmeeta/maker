package ldh.maker;

import ldh.maker.component.BootstrapContentUiFactory;
import ldh.maker.util.UiUtil;

public class BootstrapWebMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new BootstrapContentUiFactory());
        UiUtil.setType("bootstrap");
    }

    @Override
    protected String getTitle() {
        return "智能代码生成器之生成spring boot + Bootstrap前后端";
    }

    public static void main(String[] args) throws Exception {
        startDb(null);
        launch(args);
    }
}

